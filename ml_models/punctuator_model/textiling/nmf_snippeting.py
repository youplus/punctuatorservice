from ..punctuator import punctuate


def snippet(text):

	try:
		punctuatedText = punctuate.punctuateMe(text,flag=1)

		return punctuatedText
	except Exception as e:
		print(e)