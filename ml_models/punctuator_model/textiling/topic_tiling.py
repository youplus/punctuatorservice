'''
Created on Feb 28, 2019

@author: Srk
'''
from scipy import spatial
from scipy.signal import argrelextrema
from collections import namedtuple
import numpy as np

def max_left(sequence, ind_curr):
	"""
	Searches for maximum value in sequence starting from ind_curr to the left.
	
	:param sequence: List of integer values.
	:param ind_curr: Index from which search starts.
	:return integer: Maximum value from in sequence where index is less than ind_curr.
	"""
	max = sequence[ind_curr]
	while (ind_curr != 0) and (max <= sequence[ind_curr-1]):
		max = sequence[ind_curr-1]
		ind_curr -= 1 
	return max

def max_right(sequence, ind_curr):
	"""
	Searches for maximum value in sequence starting from ind_curr to the right.
	
	:param sequence: List of integer values.
	:param ind_curr: Index from which search starts.
	:return integer: Maximum value from in sequence where index is greater than ind_curr.
	"""
	max = sequence[ind_curr]
	while (ind_curr != (len(sequence)-1)) and (max <= sequence[ind_curr+1]):
		max = sequence[ind_curr+1]
		ind_curr += 1  
	return max


def fit(sentence_vectors,method = 'view'):
		"""
		Runs Topic Tiling algorithm on list of sentence vectors.
		
		:param sentence_vectors: List (iterable) of topic distributions for each sentence in document.
								 t-th element of sentence vector is weight for t-th topic for sentence, 
								 higher weight means that sentence is "more about" t-th topic.
								 
		:return: Calculated boundaries based on similatities between sentence vectors.
				 Note: boundary '0' means that segment boundary is behind sentence which is at index 0
				 in sentence_vectors.
		"""
		cosine_similarities = np.empty(0)
		depth_scores = []

		# calculating cosine similarities
		for i in range(0, len(sentence_vectors) - 1):
			sim = 1 - spatial.distance.cosine(sentence_vectors[i], sentence_vectors[i+1])
			cosine_similarities = np.append(cosine_similarities, sim)

		# get all local minima
		split_candidates_indices = argrelextrema(cosine_similarities, np.less_equal,mode='wrap')[0]

		# calculating depth scores
		for i in split_candidates_indices:
			depth = 0.5 * (max_left(cosine_similarities, i) + max_right(cosine_similarities, i) - 2 * cosine_similarities[i])
			depth_scores.append((depth, i))

		tmp = np.array(list(map(lambda d: d[0], depth_scores)))
		
		# calculate segment threshold condition
		condition = tmp.mean() - 0.2*tmp.std()
		
		# determine segment boundaries
		tmp = filter(lambda d: d[0] > condition, depth_scores)
		boundaries = [d[1] for d in tmp]
		boundary = namedtuple("boundaries", ["view", "vision"])
		return boundary(split_candidates_indices.tolist(),boundaries)