'''
Created on May 15, 2018
@author: dharmanath
'''

"""
Input file should not consist of any punctuations 
punctuations should be removed and words should be joined
e.x : good. --> good
      it's  --> its
this will help to find the starting position of segments in transcript
"""


#
import os
dir_path = os.path.dirname(os.path.realpath(__file__))

MODEL=dir_path+"/savedModel.pcl"
CONTENT_TO_FILE = True  # Content of segment
SEGMENT_TO_FILE = True  # Segment starting index
NO_OF_WORDS = 0.05      # 5%
MAX_ITERATION = 5