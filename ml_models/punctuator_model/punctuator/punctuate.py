# coding: utf-8

from __future__ import division
from nltk.tokenize import word_tokenize
from ..punctuator import models
from ..punctuator import data
import theano
import sys
import re
import theano.tensor as T
import numpy as np
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
numbers = re.compile(r'\d')
is_number = lambda x: len(numbers.sub('', x)) / len(x) < 0.6
from .. import config
model_file = None

#from django.core.cache import cache
#model_cache_key = 'model_cache' 


def setModelPath(path):
    global model_file
    model_file = path

show_unk = None
x = None
net = None
predict = None
word_vocabulary = None
punctuation_vocabulary = None
reverse_word_vocabulary = None
reverse_punctuation_vocabulary = None
human_readable_punctuation_vocabulary = None
tokenizer = None
untokenizer = None

def load_model():
    global x
    global show_unk
    global net
    global predict
    global word_vocabulary
    global punctuation_vocabulary
    global reverse_word_vocabulary
    global reverse_punctuation_vocabulary
    global human_readable_punctuation_vocabulary
    global tokenizer
    global untokenizer
    print("loading model")
    x = T.imatrix('x')
    show_unk = False

    ''' Loading model to cache '''
    # net = cache.get(model_cache_key)
    # if net is None:
    #     print "loading to cach"
    #     net, _ = models.load(model_file, 1, x)
    #     cache.set(model_cache_key, net, None) 
    # print net
    net, _ = models.load(model_file, 1, x)
    predict = theano.function(inputs=[x], outputs=net.y)
    word_vocabulary = net.x_vocabulary
    punctuation_vocabulary = net.y_vocabulary
    reverse_word_vocabulary= {v:k for k,v in net.x_vocabulary.items()}
    reverse_punctuation_vocabulary = {v:k for k,v in net.y_vocabulary.items()}
    human_readable_punctuation_vocabulary=[p[0] for p in punctuation_vocabulary if p != data.SPACE]
    tokenizer = word_tokenize
    untokenizer = lambda text: text.replace(" '", "'").replace(" n't", "n't").replace("can not", "cannot")
    

def to_array(arr, dtype=np.int32):
    return np.array([arr], dtype=dtype).T

def convert_punctuation_to_readable(punct_token):
    if punct_token == data.SPACE:
        return ' '
    elif punct_token.startswith('-'):
        return ' ' + punct_token[0] + ' '
    else:
        return punct_token[0] + ' '

def punctuate(predict, word_vocabulary, punctuation_vocabulary, reverse_punctuation_vocabulary, reverse_word_vocabulary, words, show_unk):
    
    res =""
    if len(words) == 0:
        sys.exit("Input text from stdin missing.")

    if words[-1] != data.END:
        words += [data.END]

    i = 0

    while True:

        subsequence = words[i:i+data.MAX_SEQUENCE_LEN]

        if len(subsequence) == 0:
            break

        converted_subsequence = [word_vocabulary.get(
                "<NUM>" if is_number(w) else w.lower(),
                word_vocabulary[data.UNK])
            for w in subsequence]

        if show_unk:
            subsequence = [reverse_word_vocabulary[w] for w in converted_subsequence]

        y = predict(to_array(converted_subsequence))

        #print(subsequence[0].title(),end="")
        res = res+subsequence[0].title()+""

        last_eos_idx = 0
        punctuations = []
        for y_t in y:

            p_i = np.argmax(y_t.flatten())
            punctuation = reverse_punctuation_vocabulary[p_i]

            punctuations.append(punctuation)

            if punctuation in data.EOS_TOKENS:
                last_eos_idx = len(punctuations) # we intentionally want the index of next element

        if subsequence[-1] == data.END:
            step = len(subsequence) - 1
        elif last_eos_idx != 0:
            step = last_eos_idx
        else:
            step = len(subsequence) - 1

        for j in range(step):
            current_punctuation = punctuations[j]
            #print(convert_punctuation_to_readable(current_punctuation),end="")
            res=res+convert_punctuation_to_readable(current_punctuation)
            if j < step - 1:
                if current_punctuation in data.EOS_TOKENS:
                    #print(subsequence[1+j].title(),end="")
                    res = res+subsequence[1+j].title()+""
                else:
                    #print(subsequence[1+j],end="")
                    res = res+subsequence[1+j]+""

        if subsequence[-1] == data.END:
            break

        i += step
    return res


def punctuateMe(corpus,flag=1):
    if flag==0:
        text = ' '.join(corpus)
    else:
        text = corpus
    #w for w in untokenizer(' '.join(tokenizer(text))).split()
    words = [w for w in text.split()
                if w not in punctuation_vocabulary and w not in human_readable_punctuation_vocabulary]

    res=punctuate(predict, word_vocabulary, punctuation_vocabulary, reverse_punctuation_vocabulary, reverse_word_vocabulary, words, show_unk)
    
    return res
