import pika
from  queues.producers.producers import Producer
from punctuatorServices.config import config
from punctuatorServices import log
rabbitMQIP=      config.configuration['RABBITMQCLIENT']
rabbitMQPort=     config.configuration['RABBITMQPORT']
RMQUserName=     config.configuration['RMQUserName']
RMQPassword=  config.configuration['RMQPassword']
producerExchange = config.configuration['producerExchange']
producerExchangeType=config.configuration['producerExchangeType']
logger = log.Logger()
log = logger.get_logger()


class RabbitMQProducer(Producer) :
    def initialise(self):
        
        credentials = pika.PlainCredentials(RMQUserName, RMQPassword)
        parameters = pika.ConnectionParameters(rabbitMQIP,
                                   rabbitMQPort,
                                   '/',
                                   credentials)

        self.connection = pika.BlockingConnection(parameters)
        self.channel = self.connection.channel()
  
        self.channel.exchange_declare(exchange=producerExchange, exchange_type=producerExchangeType,durable=True)
        

    def publish(self,message,routing_key):
        try:
            self.channel.basic_publish(exchange=producerExchange, routing_key=routing_key, body=message,properties=pika.BasicProperties(
                         delivery_mode = 2, # make message persistent
                      ))
        except Exception as e:
            log.error(e)
 

    def close(self):
        self.connection.close()
"""

    def initialise(self):
        credentials = pika.PlainCredentials(config['userName'], config['password'])
        parameters = pika.ConnectionParameters(config['host'],
                                   config['port'],
                                   '/',
                                   credentials)

        self.connection = pika.BlockingConnection(parameters)
        self.channel = self.connection.channel()
        self.routing_key=config['routingKey']
        exchangeOptions=config['exchangeOptions']
        self.channel.exchange_declare(exchange=config['exchangeName'], exchange_type=config['exchangeType'],durable=exchangeOptions['durable'])

    def consume(self,message):
       self.channel.basic_publish(
            exchange=config['exchangeName'], routing_key=self.routing_key, body=message)
       print(" [x] Sent %r:%r" % (self.routing_key, message))

    def close(self):
        self.connection.close()


config={}
config['userName']="test"
config['password']="test"
config['host']=""
config['port']=""
config['exchangeName']=""
config['exchangeType']=""

config['queueName']=""
config['routingKey']=""

config['exchangeOptions']={}
exchangeOptions =config['exchangeOptions']
exchangeOptions['passive']=""
exchangeOptions['durable']=""
exchangeOptions['autoDelete']=""
exchangeOptions['Internal']=""


config['queueOptions']={}
queueOptions = config['queueOptions']

queueOptions['passive']=""
queueOptions['durable']=""
queueOptions['exclusive']=""
queueOptions['autoDelete']="" """