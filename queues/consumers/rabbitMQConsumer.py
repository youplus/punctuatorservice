import pika
import queues.consumers 
from queues.consumers.consumers import Consumer
from punctuatorServices.config import config
from punctuatorServices import log
rabbitMQIP=      config.configuration['RABBITMQCLIENT']
rabbitMQPort=     config.configuration['RABBITMQPORT']
RMQUserName=     config.configuration['RMQUserName']
RMQPassword=  config.configuration['RMQPassword']
consumerExchange=config.configuration["consumerExchange"]
consumerQueue=config.configuration["consumerQueue"]
consumerExchangeType=config.configuration["consumerExchangeType"]

class RabbitMQConsumer(Consumer) :
    

    def initialise(self,routing_key):
        credentials = pika.PlainCredentials(RMQUserName, RMQPassword)
        parameters = pika.ConnectionParameters(rabbitMQIP,
                                   rabbitMQPort,
                                   '/',
                                   credentials)

        self.connection = pika.BlockingConnection(parameters)
        self.channel = self.connection.channel()
        self.channel.basic_qos(prefetch_count = 10)

        self.channel.exchange_declare(exchange=consumerExchange, exchange_type=consumerExchangeType,durable=True)
        

        result = self.channel.queue_declare(consumerQueue, exclusive=False,durable=True)
        self.queue_name = result.method.queue

       

        self.channel.queue_bind(
        exchange=consumerExchange, queue=self.queue_name, routing_key=routing_key)


    def consume(self,callback):
        self.channel.basic_consume(
            queue=self.queue_name, on_message_callback=callback)

        self.channel.start_consuming()






