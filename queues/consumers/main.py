import consumers
import consumerFactory

class Client:


    def callback(self,ch, method, properties, body):
        print(" [x] %r:%r" % (method.routing_key, body))



"""config={}
config['userName']=""
config['password']=""
config['host']=""
config['port']=""
config['exchangeName']=""
config['exchangeType']=""

config['queueName']=""
config['routingKey']=""

config['exchangeOptions']={}
exchangeOptions =config['exchangeOptions']
exchangeOptions['passive']=""
exchangeOptions['durable']=""
exchangeOptions['autoDelete']=""
exchangeOptions['Internal']=""


config['queueOptions']={}
queueOptions = config['queueOptions']

queueOptions['passive']=""
queueOptions['durable']=""
queueOptions['exclusive']=""
queueOptions['autoDelete']="" """

client=Client()
consumer = consumerFactory.getConsumer()
consumer.initialise()
consumer.consume(client.callback)