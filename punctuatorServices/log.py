import logging

class Logger(object):
    __logger = None

    @classmethod
    def get_logger(cls):
        if cls.__logger is None:
            logger = logging.getLogger()
            logger.setLevel(logging.INFO)
            hdlr = logging.FileHandler('./punctuation_services.log')
            formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
            hdlr.setFormatter(formatter)
            logger.addHandler(hdlr)            
            cls.__logger = logger
        return cls.__logger