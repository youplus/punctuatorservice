import json
import os
import punctuatorServices


global configuration
configuration = None

global ENV
ENV = None

global SENTRY
SENTRY = None

def init():
    global configuration
    global ENV
    global SENTRY
    
    config_file = os.path.dirname(punctuatorServices.__file__)+"/config/config.json"
    with open(config_file) as f:
        config = json.loads(f.read())

    ENV = config['ENV']
    SENTRY= config['SENTRY']
    if ENV == 'Ind-Dev':
        configuration = config['IND-DEV']
    elif ENV == 'Ind-Prod':
        configuration = config['IND-PROD']
    elif ENV == 'Ind-Stg':
        configuration = config['IND-STG']
    elif ENV == 'Us-Prod':
        configuration = config['US-PROD']
    else:
        raise ValueError('Invalid environment name')
init()