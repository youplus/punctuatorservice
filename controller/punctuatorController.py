from punctuatorServices import log
from models import punctuator
logger = log.Logger()
log = logger.get_logger()


class PunctuatorController:
    
    def punctuate(self,text):
            punctuatedText=punctuator.punctuateText(text)
            return punctuatedText
    
    