from controller.punctuatorController import PunctuatorController
from punctuatorServices import log
from queues.consumers import consumerFactory
from queues.producers import producerFactory

import json
import threading
import time

logger = log.Logger()
log = logger.get_logger()

class Client:
    
    def __init__(self):
      print("init")
      self.start_time=time.time()
      self.attempt_to_connect=0
        
    def process(self,message):
        try:
            punctuator=PunctuatorController()
            jsonMessage = json.loads(message)
            transformedMessage = transform(jsonMessage)
            transcribedText = transformedMessage.get("transcribed_text")

            punctuatedText = punctuator.punctuate(transcribedText)
            transformedMessage.update({'transcribed_text' : punctuatedText})
            
            self.producer= producerFactory.getProducer()
            self.producer.initialise()
            
            self.producer.publish(json.dumps(transformedMessage),"")
            self.producer.close()

        except Exception as e:
            log.error(e) 
            log.error("Snippeting failed for parent video: " + json.dumps(message))
    

    def callback(self,ch, method, proprties, body):
        self.start_time = time.time()
        self.attempt_to_connect=0

        try:
            log.info("Starting Punctuating  callback")
           
           
            
            thread = threading.Thread(target=self.process, args=(body,))
            thread.start()
            while thread.is_alive():  # Loop while the thread is processing
                ch._connection.sleep(1.0)
            log.info('Back from thread')
            
        except Exception as e:
                log.error(e)
               
        log.info("published punctuated Text")
        ch.basic_ack(delivery_tag=method.delivery_tag)

          
def main():
    client=Client()
    try:
        log.info("Starting Punctuation Service Consumer")
        client.consumer = consumerFactory.getConsumer()
        client.consumer.initialise("#")
        log.info("Punctuation Service Consumer initialised")
        client.consumer.consume(client.callback)
    except Exception as e:
        log.error(e)
        log.info(type(e))
        log.info("Calling main again")
        if(client.attempt_to_connect<10):
            log.info("Attempt number: ", client.attempt_to_connect)
            client.attempt_to_connect=client.attempt_to_connect+1
            main()
        else:
            time.sleep(600)
            client.attempt_to_connect=0
            main()

def transform(message):
    if("id" in message.keys()):
        message["_id"]= message["id"]
        del message["id"]
    if("ObjectID" in message.keys()):
        message["_id"]= message["ObjectID"]
        del message["ObjectID"]
    if("transcript" in message.keys()):
        message["transcribed_text"]= message["transcript"]
        del message["transcript"]
    if("videoUrl" in message.keys()):
        message["video_url"]= message["videoUrl"]
        del message["videoUrl"]
    return message


if __name__== "__main__":
    log.info( 'in __name__')
    main()



