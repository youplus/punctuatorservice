from ml_models.punctuator_model.punctuator import punctuate
from ml_models.punctuator_model import config
from ml_models.punctuator_model.textiling import nmf_snippeting

punctuate.setModelPath(config.MODEL)
punctuate.load_model()


def get_snippets_array(transcript):
    punctuatedText = punctuate.punctuateMe(transcript,flag=1)
    sentences = [line for line in punctuatedText.split(".") if len(line) > 1]
    print(sentences)

transcript = "an airport in paris has installed francis first security scanner the technology allows airport staff to see through passenger clothing without resorting to a strip search these passengers are being undressed a new body scanner uses recent technology that sees beneath passengers clothing these new security checks at paris's chase the goal airports are the first to be introduced in france the new scans will be available for passengers bound for the united states although the new technology is at an experimental stage and travellers can delve for a traditional body search on overture if it serves safety there's no problem i only hope that i was good looking on the pictures the images might be revealing but only one person sees them an operator of the same sex in a closed room still one passenger said she was concerned about people seeing the picture it can get by it depends if everyone can see the picture of me i would rather the physical search the technology uses millimetre waves which do not go through the body unlike x rays and the head of the french civil aviation authority says the scans sandison a selectman metric waves reflect on the skin there's no penetration in the human body that's why this no danger at it however it will reflect on objects hidden under the clothes the new scanners take just five seconds to complete their scan in which time any objects hidden on the clothing should be detected "

get_snippets_array(transcript)


