from ml_models.punctuator_model.punctuator import punctuate
from ml_models.punctuator_model import config
from ml_models.punctuator_model.textiling import nmf_snippeting

punctuate.setModelPath(config.MODEL)
punctuate.load_model()


def punctuateText(transcript):
    punctuatedText = punctuate.punctuateMe(transcript)

    return punctuatedText


